# Todo List

## https://johnsonwu.gitlab.io/todo-list

## Tools
1. Python (Flask)
2. React
3. Gitlab
4. Heroku

## 步驟
1. 在　Heroku 搭建 python 後端
- 使用 postgresql (Heroku add_ons)
  
  Local terminal
  ```=bash
  cd api
  heroku login
  heroku run python 
  ```
  python code
  ```=python
  from api import db
  db.create_all()
  ```