from flask import Flask, jsonify, request, json
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS, cross_origin

app = Flask(__name__)
CORS(app)

cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
# app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///todo.db"
app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://bcndxvceymaxma:b09bfaf4abc361376b1b4a4241981ef97e1f9ee1e4a03cbbab1716188158f94f@ec2-34-225-167-77.compute-1.amazonaws.com:5432/dfejs2u3bjbuhu"
db = SQLAlchemy(app)


class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.Text, nullable=False)

    def __str__(self):
        return f'{self.id} {self.content}'


def Todo_serializer(todo):
    return {
        'id': todo.id,
        'content': todo.content,
    }


@app.route('/api', methods=['GET'])
def index():
    return (
        jsonify([*map(Todo_serializer, Todo.query.all())])
    )


@app.route('/api/create', methods=['POST'])
def create():
    request_data = json.loads(request.data)
    todo = Todo(content=request_data['content'])

    db.session.add(todo)
    db.session.commit()

    return {'201': 'todo created successfully'}


@app.route('/api/<int:id>')
def show(id):
    return jsonify([*map(Todo_serializer, Todo.query.filter_by(id=id))])


@app.route('/api/<int:id>', methods=['POST'])
def delet(id):
    request_data = json.loads(request.data)
    Todo.query.filter_by(id=request_data['id']).delete()
    db.session.commit()

    return {'204': 'deleted successfully'}


if __name__ == '__main__':
    app.run(debug=True)
