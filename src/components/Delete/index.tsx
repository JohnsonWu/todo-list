import React from 'react'
import { useHistory } from 'react-router-dom'

const Delete = ({ id }: {id: string | undefined}) => {
  const history = useHistory()
  const deleteTodo = () => {
    fetch(`https://todo-backend-pythonflask.herokuapp.com/api/${id}`, {
      method: "POST",
      body: JSON.stringify({
        id: id,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        history.push("/todo-list/");
      });
  }
  return (
    <div>
      <button onClick={deleteTodo}>Delete</button>
    </div>
  );
}

export default Delete
