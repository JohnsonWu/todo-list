import React from 'react'

type Props = {
  userInput: string;
  onFormChange: (inputValue: string) => void;
  onFormSubmit: () => void;
};

const Form = ({ userInput, onFormChange, onFormSubmit }: Props) => {
  const handleChange = (event: any) => {
    onFormChange(event.target.value);
  };
  const handleSubmit = (event: any) => {
    event.preventDefault();
    onFormSubmit();
  };

  return (
    <form onSubmit={handleSubmit}>
      <input type="text" required value={userInput} onChange={handleChange} />
      <input type="submit" />
    </form>
  );
};

export default Form
