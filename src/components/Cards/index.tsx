import {Link} from 'react-router-dom'

const Cards = ({ listOfTodo }: {listOfTodo: Object[]}) => {
  return (
    <div>
      {listOfTodo.map((todo: any) => {
        return (
          <ul key={todo.id}>
            <li>
              <Link to={`/todo-list/${todo.id}`}>{todo.content}</Link>
            </li>
          </ul>
        );
      })}
    </div>
  );
};

export default Cards
