import React from "react";
import ToDoPage from "./pages/TodoPage";
import Show from './pages/Show'
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/todo-list/">
            <ToDoPage />
          </Route>
          <Route path="/todo-list/:id">
            <Show />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
