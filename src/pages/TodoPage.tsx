import React, { useEffect, useState } from "react";
import Cards from "./../components/Cards";
import Form from "./../components/Form";

const ToDoPage = () => {
  const [todo, setTodo] = useState<Object[]>([]);
  const [addTodo, setAddTodo] = useState<string>("");

  useEffect(() => {
    fetch("https://todo-backend-pythonflask.herokuapp.com/api")
      .then((res) => {
        if (res.ok) {
          return res.json();
        }
      })
      .then((data) => setTodo(data));
  }, []);

  const handleFormChange = (inputValue: string) => {
    setAddTodo(inputValue);
  };

  const handleFormSubmit = () => {
    fetch("https://todo-backend-pythonflask.herokuapp.com/api/create", {
      method: "POST",
      body: JSON.stringify({
        content: addTodo,
      }),
      headers: {
        "content-type": "application/json; charset=UTF-8",
      },
    })
      .then((response) => response.json())
      .then((message) => {
        console.log(message);
        setAddTodo("");
        getLatestTodos();
      });
  };

  const getLatestTodos = () => {
    fetch("https://todo-backend-pythonflask.herokuapp.com/api")
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then((data) => setTodo(data));
  }

  return (
    <div>
      <Form userInput={addTodo} onFormChange={handleFormChange} onFormSubmit={handleFormSubmit} />
      <Cards listOfTodo={todo} />
    </div>
  );
};

export default ToDoPage;
