import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Delete from './../components/Delete'

const Show = () => {
  const { id } = useParams<{ id?: string }>();
  const [todo, setTodo] = useState<any[]>([]);

  useEffect(() => {
    fetch(`https://todo-backend-pythonflask.herokuapp.com/api/${id}`)
      .then((response) => response.json())
      .then((data) => setTodo(data));
  }, [id]);

  return (
    <div>
      {todo.length > 0 && todo.map((data) => <div key={id}>{data.content}</div>)}
      <Delete id={id} />
      <hr />
      <Link to="/todo-list/">Back to todo</Link>
    </div>
  );
};

export default Show;
